from tkinter import *
from turtle import width


#definition des fonctions
'''class open_para(Button):
    def open_para(self):
        class MyButton(Button):
            
            def test_function():
                print("test")
        Fenetre_para = Tk()
        Fenetre_para.attributes('-fullscreen', True)

        frame = Frame(Fenetre_para, bg=theme, bd=1)
        frame.pack(fill="both", expand =1)
        
        label_titre = Label(Fenetre_para, text="Paramètres")
        label_titre.place(x=40, y=40)
        
        Fenetre_para.mainloop()
'''

#definition du la première fenetre
Fenetre_1 = Tk()
Fenetre_1.title("IHM_Polybot")
Fenetre_1.attributes('-fullscreen', False)
#changer la ligne du dessous pour passez de windows à une distro linux
#Sous Windows : logo.ico
#Sous linux :@/home/{le chemin vers ici(comande : pwd)}/icon.xbm
Fenetre_1.iconbitmap("@/home/charles/Documents/Polybot/IHM/interface-rasberry-pi/GUI_TkInter/logo.xbm")
theme = '#25ACD0'
Fenetre_1.config(background=theme)


box_title = Frame(Fenetre_1, bg=theme, bd=1)
label_title = Label(box_title,text="Polybot Grenbole", font=("Courrier",40), bg=theme, fg="white")
label_title.grid(row=0, column=0)



box_title.pack()



#definition du bouton paramètre
img_para=PhotoImage(file = "reglage.png").subsample(9,9)
Bouton_para = Button(Fenetre_1, image=img_para,font=("Courrier",10), bg=theme, fg="white", width=50, height=50).place(x = Fenetre_1.winfo_width()-70, y = 10)

'''Bouton_para["command"] = Bouton_para.open_para'''



#Définition des boutons

box_boutons = Frame(Fenetre_1, bg=theme, bd=1)

img_strat=PhotoImage(file = "strategie.png").subsample(4,4)
Strategie = Button(box_boutons, image=img_strat, bg=theme, fg="white", height=300, width=300).grid(row=0, column=0, padx=(100, 10))
img_debug=PhotoImage(file="tool-box.png").subsample(4,4)
Debug = Button(box_boutons, image=img_debug, bg=theme, fg="white", height=300, width=300).grid(row=0, column=1, padx=(100, 10))
box_boutons.pack(pady=100)


#afficher
Fenetre_1.mainloop()


